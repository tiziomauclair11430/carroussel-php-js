<?php include "../fonction/function.php";?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Carrousel</title>
</head>
<body>
    <h1>Ajouter votre nouvelle images</h1>
    <div>
        <form action="../action.php" method="post" enctype="multipart/form-data" name="form">
            <div id="form"></div>
            <div id="tour"></div>
            <input type="submit" value="valider">
        </form>
        <button id='btn'>Ajouter une autre images</button>
    </div> 
    <div class="rendu">
    <?php foreach(readAllCarousel() as $select){?>
        <img src="<?php echo $select['img']?>" class="img-carrousel" id="<?php echo "img".$i++ ?>">
    <?php }?>
    </div>  
</body>
<script type="text/javascript">
let form = document.getElementById('form')
let bouton = document.getElementById('btn')
let i = 1
bouton.addEventListener('click', function(){
    form.innerHTML += `<input name="img${i}" id="img${i}" type="file">`
    document.getElementById('tour').innerHTML = `<input type="hidden" name="tour" value="${i}">`;
    i++
})
</script>
</html>
