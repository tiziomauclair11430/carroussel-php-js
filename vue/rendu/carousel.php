<?php
include "../../fonction/function.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrousel</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <?php $i = 1?>
    <div class="container">
        <div id="carrousel" class="carrousel">
            <button id="gauche" class="gauche"></button>
            <?php foreach(readCarrousel(2) as $select){?>
                <img src="../<?php echo $select['img']?>" class="img-carrousel" id="<?php echo "img".$i++ ?>">
            <?php }?>
            <button id="droite" class="droite"></button>
        </div>
        <div id="pose" class="pose">
                
        </div>
        
        <input type="hidden" id="nbrImg" value="<?php echo $i - 1?>">
    </div>
</body>
<script type="text/javascript" src="caroussel.js">
</script>
</html>