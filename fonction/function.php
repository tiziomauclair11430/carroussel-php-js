<?php
include "connexion.php";
include "debug.php";
// FUNCTION ADD ===========================================
function addImage($img){
    global $pdo;
    try{
    $req = $pdo->prepare("INSERT INTO images(img) VALUES (?)");
    $req->execute([$img]);
    return $pdo->lastInsertId();
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

function addCarrousel(){
    global $pdo;
    try{
        $req = $pdo->prepare("INSERT INTO carrousel(nom) VALUES (?)");
        $req->execute([time()]);
        return $pdo->lastInsertId();
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

function liaison($id_images,$id_caroussel){
    global $pdo;
    try{
        $req = $pdo->prepare("INSERT INTO liaison(id_image,id_carrousel) VALUES (?,?)");
        $req->execute([$id_images,$id_caroussel]);
        return $pdo->lastInsertId();
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

function moveUploader($nbrImages){
    try{
        $caroussel = addCarrousel();
        for($i = 1; $i <= $nbrImages;$i++){
            $file = 'img'.$i;
            if (move_uploaded_file($_FILES[$file]['tmp_name'], "./vue/upload/".$_FILES[$file]['name'])) {
                echo "<br></br>";
                print "Téléchargé avec succès de l'image ".$i." !!!";
                $nomduFichier = "./upload/".$_FILES['img'.$i]["name"];
                $id_image = addImage($nomduFichier);
                liaison($id_image,$caroussel);
            } else {
                echo "<br></br>";
                print "Échec du téléchargement de l'image ".$i." !!!";
            }
        }
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}
// FUNCTION READ ========================================== 

function readCarrousel($id_caroussel) {
    global $pdo;
    try{
        $req = $pdo->prepare("SELECT * FROM liaison INNER JOIN images ON liaison.id_image = images.id INNER JOIN carrousel ON liaison.id_carrousel = carrousel.id WHERE carrousel.id = ?");
        $req->execute([$id_caroussel]);
        return $req->fetchAll();
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

function readAllCarousel(){
    global $pdo;
    try{
        $req = $pdo->query("SELECT * FROM liaison INNER JOIN images ON liaison.id_image = images.id INNER JOIN carrousel ON liaison.id_carrousel = carrousel.id");
        return $req->fetchAll();
    }catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

// ECHO ===================================================

// echo "<br></br>";
// echo "la page fonction.php est bien reliée";
?>