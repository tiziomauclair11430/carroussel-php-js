DROP DATABASE IF EXISTS test_carrousel;
CREATE DATABASE test_carrousel;
USE test_carrousel;
GRANT ALL PRIVILEGES ON test_carrousel.* TO 'ti'@'localhost';
flush privileges;

CREATE TABLE images (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    img VARCHAR(255)
);
CREATE TABLE carrousel (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(48)
);
CREATE TABLE liaison (
    id_image INT UNSIGNED,
    id_carrousel INT UNSIGNED,
    UNIQUE(id_image,id_carrousel),
    FOREIGN KEY (id_image) REFERENCES images(id),
    FOREIGN KEY (id_carrousel) REFERENCES carrousel(id)
);
